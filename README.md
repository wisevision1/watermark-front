# watermark-front

This is front-end part of project.

1. Instal packages:
`npm i --save`

2. Start as angular project:
`ng serve`

or build for production
`ng build --prod`

Requests will be sended to domain wich is specified in `./src/environments/address.ts`
