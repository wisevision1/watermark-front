import { Component, ViewEncapsulation,  OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { domain } from '../../environments/adress';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PageComponent implements OnInit {
  userName: '';
  selectedFile: File = null;

  imgSrc: '';

  constructor(private http: HttpClient) { }

  ngOnInit() {}

  onNameChanged(event) {
    this.userName = event.target.value;
  }

  onFileSelected(event) {
    this.selectedFile = event.target.files[0];
  }

  upload() {
    if (this.userName != null && this.userName != undefined && this.userName.length > 0 
      // &&
      // this.selectedFile === ???
    ) {
      const fd = new FormData();
      fd.append('user_name', this.userName);
      fd.append('file', this.selectedFile, this.selectedFile.name);

      // TODO: make separate service for http request
      this.http.post(domain + '/upload-image', fd)
      .subscribe( (result: any) => {
        if (result.success === true) {
          this.imgSrc = result.url;
        }
        if (result.success === false) {
          alert(result.text);
        }
      });
    }
  }
}
